<?php
foreach ($num as $t) {
    $total = $t->total;
    if ($total == null) {
        $total = 0;
    }
}
if (isset($_GET["id"])) {
    foreach ($user as $us) {
        $id = $us->id;
        $nombre = $us->name;
    }
} else {
    $id = $user->id;
    $nombre = $user->name;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #333;
    }

    li {
        float: left;
        border-right: 1px solid #bbb;
    }

    li:last-child {
        border-right: none;
    }

    li a {
        display: block;
        color: white;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }

    li a:hover:not(.active) {
        background-color: #111;
    }

    .active {
        background-color: purple;
    }

    #g-table tbody tr>td {
        height: 30px;
        padding-left: 3px;
    }

    #g-table {
        padding-left: 40px;
        margin-top: 20px;
    }

    nav>ul {
        display: flex;
        flex-direction: column;
        align-items: center;
    }
</style>

<body style="background-color:gray">
    <div id="menu">
        <ul>
            <?php
            foreach ($categorias as $categoria) {
                $id_cat = $categoria->id;
                $num_cat = $categoria->categoria;
                // echo "<li style='float:left' class='active'><a href='/user/home_cliente'>$categoria->categoria</a></li>";
                echo "<li style='float:left'><a href='/user/cargarProductosCliente?id_cat=$id_cat&id=$id'>$num_cat</a></li>";
            }
            ?>
            <li style="float:right"><a href="/user/logout">Cerrar sesión</a></li>
            <li style='float:right' class='active'><a href='/user/carrito?id=<?php echo "$id" ?>'><?php echo "($total)🛒" ?></a></li>
            <li style='float:right'><a href='/user/ver_compras?id=<?php echo "$id" ?>'>Ver compras realizadas</a></li>
        </ul>
    </div>
    <section>
        <?php
        if (isset($_GET['id_cat'])) {
            echo "";
        } else {
            echo "<h1 style='color:white;'>Hola $nombre! Selecciona una categoría de productos!</h1>";
            foreach ($cantidad_productos as $cantidad_producto) {
                if ($cantidad_producto->total == null) {
                    $cantidad_products = 0;
                } else {
                    $cantidad_products = $cantidad_producto->total;
                }
            }
            foreach ($total_orden as $ventas) {
                if ($ventas->total == null) {
                    $total_orden = 0;
                } else {
                    $total_orden = $ventas->total;
                }
            }
            echo "<tr><td><h3 style='color:purple';>Total productos adquiridos: <strong style='color:white';>$cantidad_products</strong></h3><h3 style='color:purple';>Monto total de compras realizadas: <strong style='color:white';>₡$total_orden</strong></h3></td></tr>";
        }
        ?>
    </section>
    <table align="center" class="table table-light" id="g-table">
        <form action="" method="POST" ectype="multipart/form-data">
            <tbody>
                <?php
                if (isset($_GET['id_cat'])) {
                    $cantd = 0;
                    foreach ($cantidad as $cant) {
                        $cantd = $cantidad->cantidad;
                    }
                    foreach ($productos as $producto) {
                        if ($producto->imagen == null) {
                            $imagen = "producto.png";
                            $producto->imagen = $imagen;
                        }
                        $stock = $producto->stock;
                        if ($cantd >= $stock) {
                            echo "<tr><td><img src='../../images/$producto->imagen.' width='100px' class='img-thumbnail'></td><td><h5>$producto->nombre $producto->descripcion</h5><h5>₡$producto->precio</h5></td><td><h5>" . $producto->stock . " en stock.</h5></td><td><a href='/user/vista?id_pr=" . $producto->sku . "&id_ca=" . $categoria->id . "&id=$id'><input type='button' value='Ver 👁'></a><a href='/user/addCarrito?id_cat=" . $categoria->id . "&id_p=" . $producto->sku . " &id=$id'><input type='button' value='Añadir 🛒' disabled='true'></a></td></tr>";
                        } else {
                            echo "<tr><td><img src='../../images/$producto->imagen.' width='100px' class='img-thumbnail'></td><td><h5>$producto->nombre $producto->descripcion</h5><h5>₡$producto->precio</h5></td><td><h5>" . $producto->stock . " en stock.</h5></td><td><a href='/user/vista?id_pr=" . $producto->sku . "&id_ca=" . $categoria->id . "&id=$id'><input type='button' value='Ver 👁'></a><a href='/user/addCarrito?id_cat=" . $categoria->id . "&id_p=" . $producto->sku . "&id=$id'><input type='button' value='Añadir 🛒'></a></td></tr>";
                        }
                    }
                }
                ?>
            </tbody>
        </form>
    </table>
</body>

</html>