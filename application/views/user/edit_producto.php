<?php
foreach ($productos as $producto) {
    $id = $producto->sku;
    $nombre = $producto->nombre;
    $descripcion = $producto->descripcion;
    $imagen = $producto->imagen;
    $cat = $producto->categoria;
    $stock = $producto->stock;
    $precio = $producto->precio;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body style="background-color:gray">
    <div class="container">
        <form class="form-inline" action="/user/edit_productos" method="post">
            <input type="hidden" name="txtId" placeholder="" id="txt1" require="" value=<?php echo $id ?>>
            <div class="form-group">
                <label for="">Nombre:</label>
                <input type="text" name="txtNombre" placeholder="" id="txt1" require="" value=<?php echo $nombre ?>>
            </div>
            <br>
            <div class="form-group">
                <label for="">Descripción:</label>
                <input type="text" name="txtDescripcion" placeholder="" id="txt2" require="" value=<?php echo $descripcion ?>>
            </div>
            <br>
            <div class="form-group">
                <label for="">Imagen:</label>
                <input type="file" accept="image/*" name="txtImg" placeholder="" id="txt3" require="" value=<?php echo $imagen?>>
            </div>
            <br>
            <div class="form-group">
                <label for="">Categoría:</label>
                <select name="txtCategoria">
                    <?php
                    foreach ($categorias as $categoria) {
                        echo "<option value=".$categoria->id.">".$categoria->categoria."</option>";
                    }
                    ?>
                </select>
            </div>
            <br>
            <div class="form-group">
                <label for="">Stock:</label>
                <input type="text" name="txtStock" placeholder="" id="txt6" require="" value=<?php echo $stock ?>>
            </div>
            <br>
            <div class="form-group">
                <label for="">Precio:</label>
                <input type="text" name="txtPrecio" placeholder="" id="txt7" require="" value=<?php echo $precio ?>>

            </div>
            <br>
            <input type="submit" value="Editar">
            <a href="/user/admin_productos"><input type="button" value="Cancelar"></a>
        </form>
    </div>
</body>

</html>