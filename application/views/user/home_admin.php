<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
#g-table tbody tr > td{
    border: 1px solid rgb(220,220,220);
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;

}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}

body{
    font-family: Arial, Helvetica, sans-serif;
}

form{
    background-color: black;
    margin: 0 auto;
    width: 400px;
    padding: 20px;
}

input{
    border: solid 0;
    border-radius: 3px;
}

input[type=text], input[type=password]{
    padding: 10px;
    font-size: 18px;
    outline: none;
    width: 370px;
}
input[type=submit]{
    background-color: #1E69E3;
    color: white;
    padding: 8px;
    border: none;
    width: 200px;
}
.center{
    text-align: center;
}

.opcion{
    padding: 5px 0;
}

.barra{
    background-color:rgb(152, 196, 236);
    border-radius: 4px;
    padding: 10px;
}

.seleccionado{
    background-color: rgb(33, 90, 143);
    border-radius: 4px;
    color: white;
    padding: 10px;
}

#menu{
    background-color: gray;
    padding: 10px;
}
#menu ul{
    margin: 0;
    padding: 0;
    list-style: none;
    display: inline-block;
    width: 100%;
}
#menu ul li{
    display: inline;
}
#menu ul li a{
    color: #1E69E3;
    text-decoration: none;
}
#menu ul li a:hover{
    color: rgb(227, 109, 30);
    text-decoration: none;
}
.cerrar-sesion{
    float: right;
}
</style>
<body style="background-color:gray">  
    <div id="menu">
        <ul>
            <li>Home - Administrador</li>
            <li class="cerrar-sesion"><a href="/user/logout">Cerrar sesión</a></li>
        </ul>
    </div>
    <table align="center" class="table table-light"  id="g-table" style="text-align:center;">
      <tr>
        <th>Clientes registrados </th>
        <th>Productos vendidos </th>
        <th>Total de ventas </th>
      </tr>
      <tbody>
        <?php
            foreach ($clientes_registrados as $clientes)
            {
                $clientes_regis = $clientes->total;
            }
            foreach ($productos_vendidos as $products){
                $total_products = $products->total;
            }
            foreach ($total_ventas as $ventas){
                $ventas_totales = $ventas->total;
            }
            echo "<tr><td>".$clientes_regis."</td><td>".$total_products."</td><td>".$ventas_totales."</td></tr>";
                                      
        ?>
      </tbody>
    </table>
    <div style="text-align: center;">
      <a href='/user/admin_categorias'><input type="button" value="Administrar Categorías"></a>
      <a href='/user/admin_productos'><input type="button" value="Administrar Productos"></a>
    </div>
</body>
</html>