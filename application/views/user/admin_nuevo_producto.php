<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body style="background-color:gray">
    <div class="container">
        <form class="form-inline" action="/user/new_producto" method="post">
            <input type="hidden" name="sku" placeholder="" id="txt1" require="">
            <div class="form-group">
                <label for="">Nombre:</label>
                <input type="text" name="nombre" placeholder="" id="txt1" require="">
            </div>
            <br>
            <div class="form-group">
                <label for="">Descripción:</label>
                <input type="text" name="descripcion" placeholder="" id="txt2" require="">
            </div>
            <br>
            <div class="form-group">
                <label for="">Imagen:</label>
                <input type="file" accept="image/*" name="imagen" placeholder="" id="txt3" require="">
            </div>
            <br>
            <div class="form-group">
                <label for="">Categoría:</label>
                <select name="categoria">
                    <?php
                    foreach ($categorias as $categoria) {
                        echo "<option value=" . $categoria->id . ">" . $categoria->categoria . "</option>";
                    }
                    ?>
                </select>
            </div>
            <br>
            <div class="form-group">
                <label for="">Stock:</label>
                <input type="text" name="stock" placeholder="" id="txt6" require="">
            </div>
            <br>
            <div class="form-group">
                <label for="">Precio:</label>
                <input type="text" name="precio" placeholder="" id="txt7" require="">

            </div>
            <br>
            <input type="submit" value="Agregar">
            <a href="/user/admin_productos"><input type="button" value="Cancelar"></a>
        </form>
    </div>
</body>

</html>