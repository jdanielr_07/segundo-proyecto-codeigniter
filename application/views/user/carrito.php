<?php
foreach ($user as $us) {
  $id = $us->id;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home</title>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
  }

  li {
    float: left;
    border-right: 1px solid #bbb;
  }

  li:last-child {
    border-right: none;
  }

  li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
  }

  li a:hover:not(.active) {
    background-color: #111;
  }

  li input {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    background-color: purple;
  }

  li input:hover:not(.active) {
    background-color: #111;
  }

  .active {
    background-color: purple;
  }

  #g-table tbody tr>td {
    border: 1px solid rgb(220, 220, 220);
    height: 30px;
    padding-left: 3px;
  }

  #g-table {
    padding-left: 40px;
    margin-top: 20px;
  }

  nav>ul {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
</style>

<body style="background-color:gray">
<form class="form-inline" action="/user/update_carrito" method="post">
    <div id="menu">
      <ul>
          <li style="float:left"><input type="submit" value="Actualizar carrito" name="actualizar"></li>
          <li style="float:right"><a href="/user/ver_compras">Ver compras</a></li>
          <li style="float:right"><a href="user/logout">Cerrar sesión</a></li>
          <li style="float:right"><a href="/user/home_cliente?id=<?php echo "$id" ?>">Volver</a></li>
      </ul>
    </div>
    <table align="center" class="table table-light" id="g-table">
      <tbody>
        <?php
        $monto_total = 0;
        foreach ($productos as $producto) {
          $cantidad = $producto->cantidad;
          $precio_x_producto = $cantidad * $producto->precio;
          $monto_total += $precio_x_producto;
          $id_producto_carrito = $producto->id;

          if ($producto->imagen == null) {
            $imagen = "producto.png";
            $producto->imagen = $imagen;
          }
          $page = '/user/carrito';
          echo "<tr><td><img src='../../images/$producto->imagen.' width='100px' class='img-thumbnail'></td><td><h5>$producto->nombre $producto->descripcion</h5><h5>₡$producto->precio</h5></td><td>Total productos: <input type='number' name='$producto->sku' min='1' max='$producto->stock' value=$cantidad></td><td><h5>Precio total: ₡$precio_x_producto</h5></td><td><a href='/user/delete_carrito?id_p=$producto->id&id=$id'><input type='button' value='Eliminar 🗑'></td></a></tr>";
        }
        if ($monto_total <= 0) {
          echo "<h4>No hay productos en el carrito.</h4>";
          echo "<table align='center' class='table table-light'  id='g-table'>
                      <tbody>
                          <tr><td><strong>Monto total: ₡$monto_total</strong></td><td><a href='index_cliente.php?page=cliente/orden.php'><input type='button' value='Realizar compra' disabled='true'></td></tr>
                      </tbody>
                    </table>";
        } else {
          echo "<table align='center' class='table table-light'  id='g-table'>
                <tbody>
                    <tr><td><strong>Monto total: ₡$monto_total</strong></td><td><a href='index_cliente.php?page=cliente/orden.php'><input type='button' value='Realizar compra' ></td></tr>
                </tbody>
              </table>";
        }
        ?>
  </form>
  </tbody>
  </table>
</body>

</html>