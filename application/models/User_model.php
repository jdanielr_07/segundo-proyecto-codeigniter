<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function authenticate($username, $password, $rol)
  {
    $query = $this->db->get_where('usuarios', array('name' => $username, 'password' => $password, 'rol_id' => $rol));
    if ($query->result()) {
      return $query->result()[0];
    } else {
      return false;
    }
  }
  /**
   *  Inserts a new user in the database
   *
   * @param $user  An associative array with all user data
   */
  public function insert($user)
  {
    $query = $this->db->insert('usuarios', $user);

    if ($this->db->affected_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function authenticateCarrito($id_p, $id)
  {
    $query = $this->db->get_where('carrito_compras', array('id_productos' => $id_p, 'id_usuario' => $id));
    if ($query->result()) {
      return $query->result()[0];
    } else {
      return false;
    }
  }

  public function insertCategoria($categoria)
  {
    $query = $this->db->insert('categorias', $categoria);
    if ($this->db->affected_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }
  public function insertProducto($producto)
  {
    $query = $this->db->insert('productos', $producto);
    if ($this->db->affected_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function LoadUser($id)
  {
    $query = $this->db->get_where('usuarios', array('id' => $id));
    return $query->result();
  }

  public function compras($id)
  {
    $query = $this->db->get_where('acciones_cliente', array('id_usuario' => $id));
    return $query->result();
  }

  public function LoadCategorias($id)
  {
    $query = $this->db->get_where('categorias', array('id' => $id));
    return $query->result();
  }

  public function LoadProductos($id)
  {
    $query = $this->db->get_where('productos', array('sku' => $id));
    return $query->result();
  }

  public function clientesRegistrados()
  {
    $query = 'SELECT COUNT(*) total FROM usuarios WHERE rol_id = 2';
    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  public function LoadCarrito($id)
  {
    $query = "SELECT productos.sku, productos.nombre, productos.descripcion, productos.imagen, categorias.categoria, productos.stock, productos.precio, carrito_compras.cantidad, carrito_compras.id
    FROM productos, categorias, carrito_compras 
    WHERE productos.categoria = categorias.id and productos.sku = carrito_compras.id_producto and carrito_compras.id_usuario = '$id'";
    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  public function productosVendidos()
  {
    $query = 'SELECT SUM(cantidad) total FROM acciones_cliente';
    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  public function totalVentas()
  {
    $query = 'SELECT SUM(cantidad) total FROM acciones_cliente';
    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  function update($id, $name, $lastname, $username, $password)
  {
    $this->db->where('id', $id);
    $this->db->set('name', $name);
    $this->db->set('lastname', $lastname);
    $this->db->set('username', $username);
    $this->db->set('password', $password);
    return $this->db->update('user');
  }

  function update_categoria($id, $name)
  {
    $this->db->where('id', $id);
    $this->db->set('categoria', $name);
    return $this->db->update('categorias');
  }

  function updateCarrito($id_producto, $cantidad, $id_usuario)
  {
    $this->db->where('id_producto', $id_producto);
    //$this->db->set('categoria', $name);
    return $this->db->update('categorias');
  }
  function update_producto($id, $name, $description, $nombreArchivo, $category, $stock, $precio)
  {
    $this->db->where('sku', $id);
    $this->db->set('nombre', $name);
    $this->db->set('descripcion', $description);
    $this->db->set('imagen', $nombreArchivo);
    $this->db->set('categoria', $category);
    $this->db->set('stock', $stock);
    $this->db->set('precio', $precio);
    return $this->db->update('productos');
  }
  public function delete_categoria($id)
  {
      $this->db->db_debug = false;
      if($this->db->delete('categorias', array('id' => $id))){
        return true;
      }else{
        return false;
        echo $this->db->error();
      }
  }
  public function delete_producto($id)
  {
      if($this->db->delete('productos', array('sku' => $id))){
        return true;
      }else{
        return false;
        echo $this->db->error();
      }
  }
  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function getByName($name)
  {
    $query = $this->db->get_where('user', array('name' => $name));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function delete($id)
  {
    $this->db->delete('user', array('id' => $id));
  }

  public function delete_carrito($id)
  {
    $this->db->delete('carrito_compras', array('id' => $id));
  }
  /**
   *  Get user by Id
   *
   * @param $id  The user's id
   */
  public function getById($id)
  {
    $query = $this->db->get_where('user', array('id' => $id));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }
  /**
   *  Get all users from the database
   *
   */
  public function all()
  {
    $query = $this->db->get('usuarios');
    return $query->result();
  }

  public function allCategorias()
  {
    $query = $this->db->get('categorias');
    return $query->result();
  }
  public function allProductos()
  {
    $query = 'SELECT productos.sku, productos.nombre, productos.descripcion, productos.imagen, categorias.categoria, productos.stock, productos.precio
    FROM productos, categorias 
   WHERE productos.categoria=categorias.id';

    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  public function totalOrden($id)
  {
    $query = "SELECT SUM(total_orden) as total FROM acciones_cliente where id_usuario = '$id'";
    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  public function CarritoNum($id)
  {
    $query = "SELECT SUM(cantidad) as total FROM carrito_compras WHERE id_usuario = '$id'";
    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  public function cargarProductosCliente($id)
  {
    $query = "SELECT productos.sku, productos.nombre, productos.descripcion, productos.imagen, categorias.categoria, productos.stock, productos.precio
    FROM productos, categorias 
    WHERE productos.categoria = categorias.id and categorias.id = '$id'";
    $resultados = $this->db->query($query);
    return $resultados->result();
  }
  public function cantidadCarrito($sku,$id)
  {
    $query = "SELECT cantidad FROM carrito_compras where id_producto = '$sku' and id_usuario = '$id'";
    $resultados = $this->db->query($query);
    return $resultados->result();
  }

  public function cantidadProductos($id)
  {
    $query = "SELECT SUM(cantidad) as total FROM acciones_cliente where id_usuario = '$id'";
    $resultados = $this->db->query($query);
    return $resultados->result();
  }
}
